# README #

## Usage of FSPEditor ##

### Shortcuts ###

Action               | Shortcut
---------------------|---------
Increase indentation | TAB
Decrease indentation | Shift-TAB
Censor               | Strg-D
Un-censor            | Strg-D

### External files ###

There are three external files at the moment. All of them are optional and (if they exist) can be found in the default working directory.:

* **styles.css**
    * Stylesheet that will be loaded on startup.
    * Possibles CSS-classes see below.
* **members.txt**
    * Names of all members
    * Format: One member per line. No newlines.
* **words.txt**
    * Difficult words that can be replaces programatically.
        * Format: WORD_TO_BE_REPLACED=WORD_THAT_REPLACES
    * No whitespace around the =
    * On pair of words in each line. No newlines.

**Possible CSS-Classs for styles.css**

CSS-Class | Applied to
----------|-----------
.root     | all GUI elements
.section  | textboxes containg h2-elements in the Redmine document
.headline | textboxes containing #-elements in the Redmine document
.censored | textboxes containing censored elements in the Redmine document

### Document layout ###

Every line has its meaning. Even every empty line.


```
#!

h1. [Title]

|_Datum_|_Ort_|_Beginn_|_Ende_|_Schriftführer_| // optional line; use _Schriftführerin_ for a female reporter
|[Date]|[Place]|[Starttime]|[Endtime]|[Reporter]| // only add when above line is given; otherwise don't add

h3. Anwesend

* [Name]
* [Name2] // one name is mandatory; every other name is optional; there must not be a newline between names

h3. Gäste // this whole section is optional; the "h3. Gäste" line without any guests following is valid

* [Guest1]
* [Guest2]

h2. [Agenda Item 1] // h2. must be followed by a newline and then a #-node! *-nodes are illegal.

# [Agenda Item 1.a] // a #-node must be followed (without newline) by another #-node or a child **-node
** [Text Item 1.a.1] // a **-node must be followed by another **-node or a ***-node. ****-node is illegal.
*** [Text Item 1.a.1.1] // there must not be newlines between text items
*** [Text Item 1.a.1.2]
# [Agenda Item 1.b]
# [Agenda Item 1.c]

h2. [Agenda Item 2] // empty h2. agenda items are valid

h2. [Agenda Item 3] // there must be a newline before any new h2. agenda item

# [Agenda Item 3.a]
** [Text Item 3.a.1]

```


## Setting up the IDE ##

Just checkout the files. All necessary dependencies can be found in **dependencies.txt**. Use them as maven libraries.