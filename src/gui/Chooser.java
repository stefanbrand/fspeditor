package gui;

import java.io.IOException;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Chooser<E> extends GridPane {

    private final Consumer<E> consumer;

    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private ListView<E> listView;

    public Chooser(ObservableList<E> items, Consumer<E> consumer) {
        this(items, null, consumer);
    }

    public Chooser(ObservableList<E> items, Callback<ListView<E>, ListCell<E>> cellFactory, Consumer<E> consumer) {
        this.consumer = consumer;

        FXMLLoader loader = new FXMLLoader(FSPEditor.class.getResource("/fxml/Chooser.fxml"));

        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        setCellFactory(cellFactory);
        listView.setItems(items);

        progressIndicator.setProgress(-1);
        items.addListener((ListChangeListener<E>) change -> Platform
                .runLater(() -> progressIndicator.setVisible(items.isEmpty())));

        listView.setOnMouseClicked(event -> {
            if (event.getClickCount() >= 2 && event.getButton() == MouseButton.PRIMARY) {
                chooseButtonClicked();
            }
        });
    }

    public void setCellFactory(Callback<ListView<E>, ListCell<E>> cellFactory) {
        if (cellFactory != null) {
            listView.setCellFactory(cellFactory);
        }
    }

    @FXML
    public void chooseButtonClicked() {
        E focusedItem = listView.getFocusModel().getFocusedItem();

        if (focusedItem != null) {
            consumer.accept(focusedItem);
            ((Stage) getScene().getWindow()).close();
        }
    }
}
