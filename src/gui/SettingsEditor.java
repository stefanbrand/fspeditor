package gui;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Window;
import model.Settings;

public class SettingsEditor extends GridPane {

    private final Settings setting;

    @FXML
    public TextField urlTextField;
    @FXML
    public TextField nameTextField;
    @FXML
    public TextField keyTextField;
    @FXML
    public TextField wikiNameTextField;

    public SettingsEditor(Settings setting) {
        this.setting = setting;
        FXMLLoader loader = new FXMLLoader(FSPEditor.class.getResource("/fxml/SettingsEditor.fxml"));

        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        urlTextField.setText(setting.getRestURL());
        nameTextField.setText(setting.getProjectName());
        keyTextField.setText(setting.getApiKey());
        wikiNameTextField.setText(setting.getWikiName());
    }

    public void setOwner(Window owner) {
        owner.setOnCloseRequest(event -> {
            saveClicked();
        });
    }

    @FXML
    private void saveClicked() {
        setting.setRestURL(urlTextField.getText());
        setting.setProjectName(nameTextField.getText());
        setting.setApiKey(keyTextField.getText());
        setting.setWikiName(wikiNameTextField.getText());
    }
}
