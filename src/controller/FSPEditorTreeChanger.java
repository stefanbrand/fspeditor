package controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.control.IndexRange;
import javafx.scene.control.TreeItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import jersey.repackaged.com.google.common.collect.Lists;
import model.PTextField;
import model.PTreeNode;

/**
 * Stores all the event handlers and helper methods to manipulate the text boxes in the vbox, the tree items in the
 * navigation and the event handlers for both the text fields and the tree items.
 */
public class FSPEditorTreeChanger {

    public static final int TIME_TO_JUMP_TO_END = 250;
    /**
     * Selects the <code>TreeView</code> in the navigation that is linked the text field which gains focus.
     */
    public static ChangeListener<Boolean> textFieldFocusListener =
            (observableValue, oldValue, newValue) -> Platform.runLater(() -> {
                if (!oldValue && newValue) { // only if field wasn't selected before
                    PTextField field = (PTextField) ((ReadOnlyBooleanProperty) observableValue).getBean();

                    if (field.getBoundsInParent().getHeight() != 0) { // avoid moving to field when not totally ready

                        FSPEditor.navigationStatic.getSelectionModel().select(field.getNavigationEntry());

                        Bounds bounds = FSPEditor.mainScrollPaneStatic.getViewportBounds();

                        double normalizedMinY = (Math.signum(bounds.getMinY())) * bounds.getMinY();
                        double normalizedMaxY = bounds.getMaxY() + normalizedMinY * 2;

                        double scrollHeight = FSPEditor.mainScrollPaneStatic.getContent().getBoundsInLocal().getHeight();
                        double yMax = field.getBoundsInParent().getMaxY();
                        double yMin = field.getBoundsInParent().getMinY();
                        double fieldHeight = field.getBoundsInParent().getHeight();

                        // TODO remove debug
//                        System.out.println(
//                                String.format("\n\nBounds: %.3f - %.3f (%.3f)", bounds.getMinY(), bounds.getMaxY(),
//                                        bounds.getMaxY() - bounds.getMinY()));
//                        System.out.println(String.format("Normal: %.3f - %.3f (%.3f)", normalizedMinY, normalizedMaxY,
//                                normalizedMaxY - normalizedMinY));
//                        System.out.println(String.format("Area  : %.3f - %.3f (%.3f)", yMin, yMax, yMax - yMin));

                        if (yMax > normalizedMaxY) {
                            FSPEditor.mainScrollPaneStatic.setVvalue(yMax / scrollHeight);
                        } else if (yMin < normalizedMinY) {
                            FSPEditor.mainScrollPaneStatic.setVvalue(yMin / scrollHeight);
                        }
                    }
                }
            });

    /**
     * Handles the special key processing like TAB and ENTER.
     *
     * @param keyEvent
     *         the <code>KeyEvent</code> that was fired
     */
    @FXML
    public static void handleTextFieldKeyPressedEvent(KeyEvent keyEvent) {
        PTextField textField = ((PTextField) keyEvent.getTarget());

        switch (keyEvent.getCode()) {
            case TAB:
                if (keyEvent.isShiftDown()) {
                    unindent(textField);
                    keyEvent.consume();
                } else {
                    indent(textField);
                    keyEvent.consume();
                }
                break;
            case ENTER:
                if (!"".equals(textField.getText())) {
                    addTextField(textField);
                } else {
                    unindent(textField);
                }

                keyEvent.consume();
                break;
            case UP:
                selectNextTextField(textField, true);
                break;
            case DOWN:
                selectNextTextField(textField, false);
                break;
        }
    }

    /**
     * Select the next or previous textfield in the vbox.
     *
     * @param textField
     *         the currently selected textfield
     * @param moveUp
     *         whether to move up (<code>true</code>) or down (<code>false</code>) in the list
     */
    private static void selectNextTextField(PTextField textField, boolean moveUp) {
        int oldCaret = textField.getCaretPosition();
        int index = FSPEditor.textVBoxStatic.getChildren().indexOf(textField);

        if ((moveUp && index == 0) || (!moveUp && index >= FSPEditor.textVBoxStatic.getChildren().size() - 1)) {
            return; // don't run over the end of the list
        }

        PTextField nextField = (PTextField) FSPEditor.textVBoxStatic.getChildren().get(index + (moveUp ? -1 : 1));

        textField.deselect();

        nextField.requestFocus();
        nextField.positionCaret(Math.min(oldCaret, nextField.getLength()));
    }

    /**
     * Adds a new <code>PTextField</code> as children or sibling of the given senderTextFile to the text fields vbox
     * and the internal PTree.
     * <p>
     * Note that <code>senderTextField</code> is NOT the text field that is to be added! A new text field will be
     * created.
     * If the sender is a depth-0 node the new node will be of depth 1. If the sender is depth-1 node the node will be
     * of depth 1. In every other case the new node will be of the same depth as the sender node.
     *
     * @param senderTextField
     *         the <code>PTextField</code> the ENTER key pressed event was fired from
     */
    private static void addTextField(PTextField senderTextField) {
        // get the references to the father PTreeNode and the entry in the navigation tree view
        PTreeNode father;
        int senderDepth = senderTextField.getDepth();
        TreeItem<PTreeNode> navigationEntry;

        if (senderTextField.getDepth() <= 1) {
            father = senderTextField.getpTreeNode();
        } else {
            father = senderTextField.getpTreeNode().getParent();
        }

        // create the new PTreeNode and set the references
        PTreeNode newNode = new PTreeNode(father, "");
        PTextField newTextField = newNode.getTextField();

        if (senderDepth == 0) { // create new #-node
            ObservableList<TreeItem<PTreeNode>> rootChildren = FSPEditor.navigationStatic.getRoot().getChildren();
            int fatherNavTreeIndex = rootChildren.indexOf(senderTextField.getNavigationEntry());

            navigationEntry = new TreeItem<>(newNode);

            FSPEditor.navigationStatic.getRoot().getChildren().get(fatherNavTreeIndex).getChildren()
                    .add(0, navigationEntry);
        } else { // create **-node
            navigationEntry = senderTextField.getNavigationEntry(); // same navigation entry as father
        }

        newTextField.setNavigationEntry(navigationEntry);
        newTextField.setCensored(father.isCensored());
        newTextField.setDepth((senderDepth <= 1) ? senderDepth + 1 : senderDepth);

        // If depth == 1 then a #-node was selected. Insert the new node as first element in the children list.
        if (senderDepth <= 1) {
            father.getChildren().add(0, newNode);
        } else {
            PTreeNode predecessor = senderTextField.getpTreeNode();
            int predecessorIndex = father.getChildren().indexOf(predecessor);

            father.getChildren().add(predecessorIndex + 1, newNode);

            // children of the predecessor are now children of the new node
            newNode.getChildren().addAll(predecessor.getChildren());
            predecessor.getChildren().clear();
        }

        // insert into vbox
        int index = FSPEditor.textVBoxStatic.getChildren().indexOf(senderTextField) + 1;
        FSPEditor.textVBoxStatic.getChildren().add(index, newTextField);
        newTextField.deselect();
        newTextField.requestFocus();
        newTextField.positionCaret(senderTextField.getLength());

        if (index == FSPEditor.textVBoxStatic.getChildren().size() - 1) {
            Timer t = new Timer();
            t.schedule(new TimerTask() {

                @Override
                public void run() {
                    Platform.runLater(() -> FSPEditor.mainScrollPaneStatic.setVvalue(1));
                }
            }, TIME_TO_JUMP_TO_END);
        }
    }

    /**
     * Indents the given <code>textField</code> by one. All the children of the <code>PTreeNode</code> that is linked
     * to the <code>textField</code> are indented as well. The index in the vbox may change and if the node was a
     * #-node
     * and becomes a
     * **-node, the navigation tree will be updated.
     *
     * @param textField
     *         the textfield to indent
     */
    private static void indent(PTextField textField) {
        // intending of structure entries is not supported
        if (textField.getDepth() == 0) {
            return;
        }

        /*
         * Try to find the predecessor of the node in the same level and add this node to the predecessors list of
         * children.
         * If there is no predecessor then indentation is not possible.
         */
        PTreeNode ownNode = textField.getpTreeNode();
        PTreeNode father = textField.getpTreeNode().getParent();
        int ownIndex = father.getChildren().indexOf(ownNode);

        if (ownIndex == 0) { // without predecessor there cant be a new father
            return;
        }

        PTreeNode newFather = father.getChildren().get(ownIndex - 1); // predecessor is new father

        newFather.getChildren().add(ownNode);
        father.getChildren().remove(ownIndex);

        ownNode.setParent(newFather);
        textField.increaseDepth();
        textField.positionCaret(textField.getLength());

        // find new position in vbox for children
        indentAllChildren(ownNode);

        // update navigation tree if node is now a **-node
        if (textField.getDepth() == 2) {
            TreeItem<PTreeNode> oldNavEntry = textField.getNavigationEntry();
            int oldNavIndex = oldNavEntry.getParent().getChildren().indexOf(oldNavEntry);
            TreeItem<PTreeNode> newNavEntry = oldNavEntry.getParent().getChildren().get(oldNavIndex - 1);

            ownNode.getTextField().setNavigationEntry(newNavEntry);
            oldNavEntry.getParent().getChildren().remove(oldNavEntry);
        }
    }

    /**
     * Increases the depth of all the children of the given <code>node</code> by one. Does not increase the
     * <code>node</code> itself.
     *
     * @param node
     *         the <code>PTreeNode</code> to increase the children indention of
     */
    private static void indentAllChildren(PTreeNode node) {
        node.getChildren().forEach((child) -> {
            child.getTextField().increaseDepth();
            indentAllChildren(child);
        });
    }

    /**
     * Unindents the given <code>textField</code> by one. All the children of the <code>PTreeNode</code> that is linked
     * to the <code>textField</code> are unindented as well. The index in the vbox may change and if the node becomes a
     * #-node, the navigation tree will be updated.
     *
     * @param textField
     *         the textfield to unindent
     */
    private static void unindent(PTextField textField) {
        // unindenting to structure entries is not supported
        if (textField.getDepth() <= 1) {
            return;
        }

        PTreeNode ownNode = textField.getpTreeNode();
        PTreeNode oldFather = textField.getpTreeNode().getParent();

        PTreeNode newFather = oldFather.getParent();
        int oldFatherIndex = newFather.getChildren().indexOf(oldFather);
        int ownIndexInOldFather = oldFather.getChildren().indexOf(ownNode);

        // unindent the old children (new ones are not added yet)
        unindentAllChildren(ownNode);

        // add the children of old father that come after ownNode to ownNodesChildren
        List<PTreeNode> movedNodes = new LinkedList<>();
        for (int i = ownIndexInOldFather + 1; i < oldFather.getChildren().size(); i++) {
            PTreeNode nodeToBeMoved = oldFather.getChildren().get(i);
            ownNode.getChildren().add(nodeToBeMoved);
            movedNodes.add(nodeToBeMoved);
        }

        for(PTreeNode movedNode : movedNodes) {
            oldFather.getChildren().remove(movedNode);
        }

        // move ownNode from old to new father
        newFather.getChildren().add(oldFatherIndex + 1, ownNode); // add ownNode after old father
        oldFather.getChildren().remove(ownNode);

        ownNode.setParent(newFather);
        textField.decreaseDepth();
        textField.positionCaret(textField.getLength());

        // update navigation tree if node is now a #-node
        if (textField.getDepth() == 1) {
            TreeItem<PTreeNode> oldNavEntry = textField.getNavigationEntry();
            TreeItem<PTreeNode> newNavEntry = new TreeItem<>(ownNode);
            int oldNavIndex = oldNavEntry.getParent().getChildren().indexOf(oldNavEntry);

            ownNode.getTextField().setNavigationEntry(newNavEntry);
            oldNavEntry.getParent().getChildren().add(oldNavIndex + 1, newNavEntry);
        }
    }

    /**
     * Moves the <code>textField</code> to the given <code>newIndex</code>.
     *
     * @param textField
     *         the <code>PTextField</code> that is to be moved
     * @param newIndex
     *         the newIndex where the <code>textField</code> is to be moved to
     */
    private static void moveTextField(PTextField textField, int newIndex) {
        FSPEditor.textVBoxStatic.getChildren().remove(textField);
        FSPEditor.textVBoxStatic.getChildren().add(newIndex, textField);
    }

    /**
     * Moves all the children of the given parent <code>node</code> to follow the father's <code>nodeIndex</code>.
     *
     * @param node
     *         the parent node
     * @param nodeIndex
     *         the parent's textbox index in the vbox
     *
     * @return the index of the last node that was moved (the very last child in the children tree of the
     * <code>node</code>)
     */
    private static int moveAllChildren(PTreeNode node, int nodeIndex) {
        for (PTreeNode child : node.getChildren()) {
            moveTextField(child.getTextField(), nodeIndex);
            nodeIndex = moveAllChildren(child, nodeIndex + 1);
        }

        return nodeIndex;
    }

    /**
     * Decreases the depth of all the children of the given <code>node</code> by one. Does not decrease the
     * <code>node</code> itself.
     *
     * @param node
     *         the <code>PTreeNode</code> to decrease the children indention of
     */
    private static void unindentAllChildren(PTreeNode node) {
        node.getChildren().forEach((child) -> {
            child.getTextField().decreaseDepth();
            unindentAllChildren(child);
        });
    }

    @FXML
    public static void handleTextFieldKeyTypedEvent(KeyEvent keyEvent) {
        PTextField textField = ((PTextField) keyEvent.getTarget());

        if (!keyEvent.isConsumed() && textField.getDepth() == 1) {
            TreeItem<PTreeNode> navTreeItem = textField.getNavigationEntry();
            TreeItem<PTreeNode> navTreeItemFather = navTreeItem.getParent();
            int navTreeItemIndex = navTreeItemFather.getChildren().indexOf(navTreeItem);

            textField.getpTreeNode().setName(textField.getText());

            navTreeItemFather.getChildren().remove(navTreeItemIndex);

            navTreeItemFather.getChildren().add(navTreeItemIndex, navTreeItem);
            FSPEditor.navigationStatic.getSelectionModel().select(navTreeItem);
        }

        if (keyEvent.getCode() == KeyCode.D && keyEvent.isControlDown()) {
            censorTextField(keyEvent, textField);
        } else if (keyEvent.getCode() == KeyCode.BACK_SPACE) {
            if (textField.getText().isEmpty()) {
                deleteTextField(textField);
            }
        }
    }

    private static void deleteTextField(PTextField textField) {
        if (textField.getDepth() == 0) {
            return; // deletion of h2 agenda items is not supported
        } else if (textField.getDepth() == 1) {
            TreeItem<PTreeNode> navigationEntry = textField.getNavigationEntry();

            navigationEntry.getParent().getChildren().remove(navigationEntry);
        }

        PTreeNode ownNode = textField.getpTreeNode();
        PTreeNode parent = ownNode.getParent();
        int ownIndex = parent.getChildren().indexOf(ownNode);

        parent.getChildren().remove(ownNode);

        if (ownIndex == 0) { // ownNode was directly under parent
            // add children on the start of the parents child list
            Lists.reverse(ownNode.getChildren()).forEach(child -> parent.getChildren().add(0, child));
            unindentAllChildren(ownNode);
        } else { // ownNode's children are now children of the previous sibling
            parent.getChildren().get(ownIndex - 1).getChildren().addAll(ownNode.getChildren());
        }

        // try to select another textnode
        if (FSPEditor.textVBoxStatic.getChildren().size() > 1) {
            int oldSelectionIndex = FSPEditor.textVBoxStatic.getChildren().indexOf(textField);
            int newSelectionIndex;

            if (oldSelectionIndex == 0) {
                newSelectionIndex = 1;
            } else {
                newSelectionIndex = oldSelectionIndex - 1;
            }

            textField.deselect();

            PTextField newSelection = (PTextField) FSPEditor.textVBoxStatic.getChildren().get(newSelectionIndex);

            newSelection.requestFocus();
            newSelection.positionCaret(newSelection.getText().length());
        }

        FSPEditor.textVBoxStatic.getChildren().remove(textField);
    }

    private static void censorTextField(KeyEvent keyEvent, PTextField textField) {
        keyEvent.consume();

        if (textField.getSelection().getLength() == 0) { // censor whole node and children if no text selected
            PTreeNode pTreeNode = textField.getpTreeNode();

            pTreeNode.toggleCensored();
            pTreeNode.propagateCensored();
        } else { // just (un)censor the selection
            boolean censor = true;

            String text = textField.getText();
            String selection = textField.getSelectedText();
            IndexRange range = textField.getSelection();

            // figure out whether to censor or un-censor
            if (selection.charAt(0) == '-' || selection.charAt(range.getLength() - 1) == '-' || (range.getStart() != 0
                    && text.charAt(range.getStart() - 1) == '-') || (range.getEnd() < text.length() - 1
                    && text.charAt(range.getEnd()) == '-')) {
                censor = false;
            }

            System.out
                    .println((censor ? "Censor" : "Un-Censor") + " from " + range.getStart() + " to " + range.getEnd());

            if (censor) {
                String leadingSpace = " ";
                String closingSpace = " ";

                // make sure there is a space in front of the leading - and a space behind the closing -
                if (range.getStart() == 0 || text.charAt(range.getStart() - 1) == ' ') {
                    leadingSpace = "";
                }

                if (range.getEnd() == text.length() || text.charAt(range.getEnd()) == ' ') {
                    closingSpace = "";
                }

                String newSelection = leadingSpace + "-" + selection.trim() + "-" + closingSpace;

                textField.replaceSelection(newSelection);
            } else {
                int newStart = range.getStart();
                int newEnd = range.getEnd();

                if (selection.charAt(0) != '-') {
                    newStart--;
                }

                if (selection.charAt(range.getLength() - 1) != '-') {
                    newEnd++;
                }

                textField.selectRange(newStart, newEnd);

                text = textField.getSelectedText();

                textField.replaceSelection(text.substring(1, text.length() - 1));
            }
        }
    }
}
