package controller;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import gui.Chooser;
import gui.SettingsEditor;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;
import model.*;
import org.cdmckay.coffeedom.Element;
import org.controlsfx.dialog.Dialogs;

public class FSPEditor {

    public static final FileChooser.ExtensionFilter REDMINE_FILTER =
            new FileChooser.ExtensionFilter("Redmine File", "*.redmine");
    public static final String FILENAME_MEMBER = "members.txt";

    // Static references to GUI elements for event handlers
    /**
     * Static reference for event handlers.
     */
    public static TreeView<PTreeNode> navigationStatic;
    /**
     * Static reference for event handlers.
     */
    public static VBox textVBoxStatic;
    public static ScrollPane mainScrollPaneStatic;
    public static boolean isBigMode = false;

    // References to GUI elements
    public VBox textVBox;
    public TreeView<PTreeNode> navigation;
    public ListView<String> guestList;
    public VBox presentMembersVBox;
    public TextField newMemberField;
    public TextField newGuestField;
    public TextField reporter;
    public SplitPane mainSplitpane;
    public TextField fromTime;
    public TextField toTime;
    public TextField place;
    public TextField date;
    public CheckBox femaleReporter;
    public ScrollPane mainScrollPane;
    public GridPane rootGridPane;

    private Settings settings;

    /**
     * The Difficult words. The key is the "shortcut-word" to replace and value the word that replaces the key.
     */
    public Map<String, String> words;

    /**
     * All members - attending or not attending.
     */
    public List<String> members;

    /**
     * The present members. Subset of the members list.
     */
    public List<String> presentMembers;

    /**
     * The list with the guests. Is linked to the internal list in the <code>guestList</code> list view.
     */
    public ObservableList<String> guests;

    /**
     * The scene where the main gui is held in.
     */
    private Window owner;

    /**
     * The metadata of the redmine document.
     */
    private DocumentMetadata metadata;

    /**
     * The position the the divider in the main split pane.
     */
    private double dividerPosition;

    /**
     * Whether the new divider position is stored in the <code>dividerPosition</code> variable in case the divider's
     * position changes. If <code>true</code> it is not stored.
     */
    private boolean lockDivider = false;

    /**
     * The redmine document in case it was loaded from disk or has already been saved.
     */
    private File protocolFile;

    /**
     * Initializes the GUI by adding a default root element to the navigation tree view.
     */
    @FXML
    public void initialize() {
        navigationStatic = navigation;
        textVBoxStatic = textVBox;
        mainScrollPaneStatic = mainScrollPane;
        dividerPosition = mainSplitpane.getDividerPositions()[0];
        navigation.setOnMouseClicked(event -> {
            TreeItem<PTreeNode> selectedItem = navigation.getSelectionModel().getSelectedItem();

            if (selectedItem != null) {
                PTextField textField = selectedItem.getValue().getTextField();
                textField.requestFocus();
                textField.deselect();
                textField.positionCaret(textField.getLength());
            }
        });

        guests = guestList.getItems();
        presentMembers = new LinkedList<>();

        loadSettings();
        loadWords();
        loadMembers();

        // TODO remove debug
        Pair<TreeItem<PTreeNode>, DocumentMetadata> parsedPair = RedmineParser
                .parse("h1. 4. Sitzung am 06. Mai 2014\n" + "\n" + "|_Datum_|_Ort_|_Beginn_|_Ende_|_Schriftführerin_|\n"
                        + "|30.5.2014|IM 242|18:15|20:00|Ramona|\n" + "\n" + "h3. Anwesend\n" + "\n" + "* Niko\n"
                        + "* Stefan\n" + "* Ramona\n" + "\n" + "h3. Gäste\n" + "\n" + "* Chuck Norris\n" + "\n"
                        + "h2. Beschließen der Tagesordnung und aktuelle Anliegen\n" + "\n" + "h2. Berichte\n" + "\n"
                        + "# Kurzbericht von der Planung der Auslandsinfoveranstaltung (_Marco_)\n"
                        + "* sollte ignoriert werden\n" + "das hier auch\n"
                        + "# Kurzbericht vom Spontangespräch mit Dr. Offinger bzgl. Englisch FFA HS (_Marco_)\n"
                        + "** a\n" + "** b\n" + "*** b.1\n" + "*** -b.2- nur der Anfang zensiert!\n" + "**** b.2.1\n"
                        + "** c\n" + "*** -c.1 zensiert-\n" + "**** -c.1.2 selbst zensiert und parent zensiert-\n"
                        + "*** c.2\n" + "**** -c.2.1 selbst zensiert-\n"
                        + "***** c.2.1.1 zensiert, da parent zensiert\n"
                        + "# Bericht vom [[berichte:6. Studiendekankaffee|6. Studiendekankaffee am 06.05]] " +
                        "(_Lisa_, _Tommy_)\n" + "*** illegal\n" + "**** illegal kind\n" + "****** nochmal illegal",
                        presentMembers, guests);

        // Add present member to members if not already in list
        presentMembers.forEach(member -> {
            if (!members.contains(member)) {
                members.add(member);
            }
        });

        metadata = parsedPair.getValue();

        date.setText(metadata.getDate());
        place.setText(metadata.getPlace());
        fromTime.setText(metadata.getFrom());
        toTime.setText(metadata.getTo());
        reporter.setText(metadata.getReporter());
        femaleReporter.setSelected(metadata.isFemaleReporter());

        navigation.setRoot(parsedPair.getKey());
        parsedPair.getKey().getChildren().forEach(node -> createTextFields(node.getValue(), 0));

        updatePresentMembers();

        updateStyle(null);
    }

    private void updatePresentMembers() {
        presentMembersVBox.getChildren().clear();

        members.forEach(member -> {
            CheckBox checkBox = new CheckBox(member);
            checkBox.setOnAction(event -> readPresentMembers());

            presentMembersVBox.getChildren().add(checkBox);
        });

        presentMembers.forEach(presentMember -> {
            presentMembersVBox.getChildren().forEach(member -> {
                CheckBox m = (CheckBox) member;
                if (m.getText().equals(presentMember)) {
                    m.setSelected(true);
                    return; // early exit
                }
            });
        });
    }

    /**
     * Loads content of <code>membersFile</code> into <code>observableMembers</code> if it exists.
     * If there's no file, a new one will be created.
     */
    private void loadMembers() {
        File membersFile = new File(FILENAME_MEMBER);

        members = new LinkedList<>();

        if (!membersFile.exists()) {
            System.out.println("Members file " + membersFile.getAbsolutePath() + " does not exist.");

            return;
        }

        try {
            members = Files.readAllLines(membersFile.toPath(), StandardCharsets.UTF_8);
            members.forEach(member -> member = member.trim());
            members.removeIf(""::equals);
            System.out
                    .printf("Members file %s read. %d members found.%n", membersFile.getAbsolutePath(), members.size());
        } catch (IOException e) {
            members = new LinkedList<>();
            System.out.println("Members file could not be read.");
            e.printStackTrace();
        }
    }

    /**
     * Loads content of <code>wordsFile</code> into <code>words</code> if it exists.
     */
    private void loadWords() {
        File wordsFile = new File(FSPEditorWords.FILENAME);

        words = new LinkedHashMap<>();

        if (!wordsFile.exists()) {
            System.out.println("Words file " + wordsFile.getAbsolutePath() + " does not exit.");
            return;
        }

        try {
            List<String> lines = Files.readAllLines(wordsFile.toPath(), StandardCharsets.UTF_8);

            for (String line : lines) {
                String[] parts = line.trim().split("=");

                if (parts.length != 2) {
                    System.out.println("Illegal line in words: " + line);
                    continue;
                }

                words.put(parts[0].trim(), parts[1].trim());
            }
        } catch (IOException e) {
            System.out.println("Error while reading the words file.");
            e.printStackTrace();
        }
    }

    private void loadSettings() {
        File settingsFile = new File(Settings.FILENAME);
        // TODO exception handling

        if (settingsFile.exists()) {
            settings = Settings.load();
        }

        if (settings == null) {
            settings = new Settings();
        }
    }

    private void loadRedmineXML(Element element) {
        presentMembers.clear();
        guests.clear();

        Pair<TreeItem<PTreeNode>, DocumentMetadata> parsedPair =
                RedmineParser.parse(element.getChildText(RedmineWikiHandler.XML_TEXT), presentMembers, guests);

        metadata = parsedPair.getValue();

        date.setText(metadata.getDate());
        place.setText(metadata.getPlace());
        fromTime.setText(metadata.getFrom());
        toTime.setText(metadata.getTo());
        reporter.setText(metadata.getReporter());
        femaleReporter.setSelected(metadata.isFemaleReporter());

        textVBox.getChildren().clear();

        navigation.setRoot(parsedPair.getKey());
        parsedPair.getKey().getChildren().forEach(node -> createTextFields(node.getValue(), 0));

        updatePresentMembers();
    }

    /**
     * Adds the <code>PTextField</code> stored in <code>father</code> and all its children to the vbox.
     *
     * @param parent
     *         the <code>PTreeNode</code> which contains the text field to add
     * @param fatherDepth
     *         the depth of the node
     */
    private void createTextFields(PTreeNode parent, int fatherDepth) {
        PTextField textField = parent.getTextField();

        textVBox.getChildren().add(textField);

        parent.getChildren().forEach((node) -> createTextFields(node, fatherDepth + 1));
    }

    /**
     * Open a new window to select a word from the list of words with a difficult
     * spelling. It's also possible to add or delete words.
     *
     * @throws IOException
     */
    @FXML
    public void wordAddButtonClicked() throws IOException {
        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FSPEditorWords.fxml"));
        Parent parent = loader.load();

        loader.<FSPEditorWords>getController().setWords(words);

        Scene scene = new Scene(parent);

        stage.setTitle("Wörterbuch");
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);

        stage.show();
    }

    /**
     * Saves the protocol. If no protocol file has been set yet it shows the "Save as" dialogue.
     */
    @FXML
    public void saveButtonClicked() {
        if (protocolFile != null) {
            saveFile(protocolFile);
        } else {
            saveAsButtonClicked();
        }
    }

    /**
     * Opens a saving dialogue to save the protocol to a specific path. The default path is the home directory of the
     * user.
     */
    @FXML
    public void saveAsButtonClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Protokoll speichern unter...");

        if (protocolFile != null && protocolFile.getParentFile() != null) {
            fileChooser.setInitialDirectory(protocolFile.getParentFile());
        } else {
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }

        fileChooser.getExtensionFilters().add(REDMINE_FILTER);

        Stage stage = new Stage();
        protocolFile = fileChooser.showSaveDialog(stage);

        if (protocolFile != null) {
            saveFile(protocolFile);
        }
    }

    private void saveFile(File file) {
        try {
            Files.write(Paths.get(file.getAbsolutePath()), getRedmineString().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            System.err.println("Failed to write"); // TODO notify user
        } catch (EmptyDocumentException e) {
            System.err.println("Document was empty!"); // TODO notify user
        }
    }

    /**
     * Downloads the structure of the protocol from Redmine.
     */
    @FXML
    public void downloadButtonClicked() {

        if (!settings.allSettingsGiven()) {
            String message = "Invalid settings for Redmine download.";
            Dialogs.create().owner(owner).message(message).showError();
            return;
        }

        ObservableList<Element> pages = FXCollections.observableArrayList();
        RedmineWikiHandler loader = new RedmineWikiHandler(settings);
        Chooser<Element> chooser = new Chooser<>(pages, this::loadRedmineXML);

        chooser.setCellFactory(view -> new ListCell<Element>() {

            @Override
            protected void updateItem(Element element, boolean empty) {
                super.updateItem(element, empty);

                if (!empty) {
                    setText(element.getChildText(RedmineWikiHandler.XML_TITLE));
                }
            }
        });

        Stage stage = new Stage();
        Scene scene = new Scene(chooser);

        stage.setScene(scene);
        stage.setTitle("Tagesordnung herunterladen...");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);
        stage.show();

        new Thread(() -> pages.addAll(loader.loadConferenceWikiPages())).start();
    }

    /**
     * Uploads the protocol as a wiki page to Redmine.
     */
    @FXML
    public void uploadButtonClicked() {
        String title = Dialogs.create().owner(owner).lightweight().showTextInput();

        if (title == null) {
            return;
        }

        final String normalized = title.replaceAll("\\s+", "_").replaceAll("\\.", "");

        try {
            final String redmineContent;
            redmineContent = getRedmineString();
            RedmineWikiHandler wikiHandler = new RedmineWikiHandler(settings);

            new Thread(() -> wikiHandler.pushPage(normalized, redmineContent)).start();
        } catch (EmptyDocumentException e) {
            System.err.println("Document was empty"); // TODO notify user
        }
    }

    /**
     * Closes the application.
     */
    @FXML
    public void closeButtonClicked() {
        Platform.exit();
    }

    /**
     * Shows a dialogue to open a protocol.
     */
    @FXML
    public void openButtonClicked() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();

        fileChooser.setTitle("Protokoll öffnen");

        if (protocolFile != null && protocolFile.getParentFile() != null) {
            fileChooser.setInitialDirectory(protocolFile.getParentFile());
        } else {
            fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }

        fileChooser.getExtensionFilters().add(REDMINE_FILTER);

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            openFile(file);
        }
    }

    private void openFile(File file) {
        try {
            if (!file.exists()) {
                System.out.println("Redmine document: " + file.getAbsolutePath() + " doesn't exist.");
                return;
            }

            String redmine = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);

            protocolFile = file;

            textVBox.getChildren().clear();
            presentMembers.clear();
            guests.clear();

            Pair<TreeItem<PTreeNode>, DocumentMetadata> parsedPair =
                    RedmineParser.parse(redmine, presentMembers, guests);

            // Add present member to members if not already in list
            presentMembers.forEach(member -> {
                if (!members.contains(member)) {
                    members.add(member);
                }
            });

            members.sort(String::compareTo);

            metadata = parsedPair.getValue();

            date.setText(metadata.getDate());
            place.setText(metadata.getPlace());
            fromTime.setText(metadata.getFrom());
            toTime.setText(metadata.getTo());
            reporter.setText(metadata.getReporter());
            femaleReporter.setSelected(metadata.isFemaleReporter());

            navigation.setRoot(parsedPair.getKey());
            navigation.getRoot().getChildren().forEach(node -> createTextFields(node.getValue(), 0));

            updatePresentMembers();
        } catch (IOException e) {
            System.err.println("Failed to open redmine file at " + file.getAbsolutePath());
            // TODO show error to user
        }
    }

    @FXML
    public void preferencesButtonClicked() {
        Stage stage = new Stage();
        SettingsEditor settingsEditor = new SettingsEditor(settings);
        Scene scene = new Scene(settingsEditor);

        stage.setScene(scene);
        stage.setTitle(SettingsEditor.class.getSimpleName());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);

        settingsEditor.setOwner(scene.getWindow());

        stage.show();
    }

    public void setOwner(Window owner) {
        this.owner = owner;

        owner.setOnCloseRequest(event -> {
            try {
                settings.save();
            } catch (IOException e) {
                System.err.println("Could not save the settings. " + e);
            }

            if (!words.isEmpty()) {
                try {
                    Path path = new File(FSPEditorWords.FILENAME).toPath();
                    List<String> lines = new ArrayList<>();

                    for (Map.Entry<String, String> word : words.entrySet()) {
                        lines.add(word.getKey() + "=" + word.getValue());
                    }

                    Files.write(path, lines, StandardCharsets.UTF_8);
                } catch (IOException e) {
                    System.err.println("Could not save the words list. " + e);
                }
            }

            if (!members.isEmpty()) {
                try {
                    Path path = new File(FILENAME_MEMBER).toPath();

                    Files.write(path, members, StandardCharsets.UTF_8);
                } catch (IOException e) {
                    System.err.println("Could not save the members list. " + e);
                }
            }
        });

        // try to keep the split pane divider in place while resizing the window
        mainSplitpane.getDividers().get(0).positionProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println(newValue.doubleValue());
            if (!lockDivider) {
                dividerPosition = newValue.doubleValue();
            }
        });
        owner.getScene().widthProperty().addListener(event -> {
            lockDivider = true;
            Platform.runLater(() -> {
                lockDivider = false;
                mainSplitpane.setDividerPosition(0, dividerPosition);
            });
        });
    }

    private String getRedmineString() throws EmptyDocumentException {
        if (metadata == null || navigation.getRoot() == null) {
            throw new EmptyDocumentException();
        }

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("h1. %s\n", metadata.getTitle()));

        sb.append(String.format("\n|_Datum_|_Ort_|_Beginn_|_Ende_|_Schriftführer%s_|\n" + "|%s|%s|%s|%s|%s|\n\n",
                femaleReporter.isSelected() ? "in" : "", date.getText().trim(), place.getText().trim(),
                fromTime.getText().trim(), toTime.getText().trim(), reporter.getText().trim()));

        sb.append("h3. Anwesend\n\n");
        presentMembers.forEach(member -> sb.append(String.format("* %s\n", member)));

        if (!guests.isEmpty()) {
            sb.append("\nh3. Gäste\n\n");
            guests.forEach(guest -> sb.append(String.format("* %s\n", guest)));
        }

        sb.append("\n");

        navigation.getRoot().getChildren().forEach(node -> sb.append(node.getValue().toRedmine()).append("\n\n"));

        return sb.toString();
    }

    @FXML
    public void addNewGuest() {
        String newGuest = newGuestField.getText().trim();

        if (!newGuest.isEmpty()) {
            String lowerCaseGuest = newGuest.toLowerCase();

            if (guests.stream().map(String::toLowerCase).anyMatch(guest -> guest.equals(lowerCaseGuest))) {
                Point2D textFieldCoords = newGuestField.localToScreen(0, newGuestField.getHeight());

                newGuestField
                        .setTooltip(new Tooltip("Diesen Namen gibt's schon!")); // TODO use same dialog in member list
                newGuestField.getTooltip().show(newGuestField, textFieldCoords.getX(), textFieldCoords.getY());
                newGuestField.getTooltip().setAutoHide(true);
            } else {
                newGuestField.setTooltip(null);

                guests.add(newGuest);

                newGuestField.setText(""); //clear text field to add more members easily
            }
        }
    }

    @FXML
    public void addNewMember() {
        String newMember = newMemberField.getText().trim();

        if (!newMember.isEmpty()) {
            if (!members.contains(newMember)) {
                members.add(newMember);

                if (!presentMembers.contains(newMember)) {
                    presentMembers.add(newMember);
                }

                members.sort(String::compareTo);
                presentMembers.sort(String::compareTo);

                updatePresentMembers();
            } else {
                Point2D textFieldCoords = newMemberField.localToScreen(0, newMemberField.getHeight());
                newMemberField.setTooltip(new Tooltip("Diesen Namen gibt's schon!"));
                newMemberField.getTooltip().show(newMemberField, textFieldCoords.getX(), textFieldCoords.getY());
                newMemberField.getTooltip().setAutoHide(true);
                return;
            }

            newMemberField.setTooltip(null);
            newMemberField.setText(null); //clear text field to add more members easily
        }
    }

    /**
     * Updates and sorts the <code>presentMembers</code> list to the current selection in the
     * <code>presentMembersVBox</code>.
     */
    public void readPresentMembers() {
        presentMembers.clear();

        presentMembersVBox.getChildren().forEach(member -> {
            CheckBox m = (CheckBox) member;

            if (m.isSelected()) {
                presentMembers.add(m.getText());
            }
        });

        presentMembers.sort(String::compareTo);
    }

    @FXML
    public void femaleReporterChanged(ActionEvent actionEvent) {
        if (((CheckBox) actionEvent.getSource()).isSelected()) {
            reporter.setPromptText("Schriftführerin");
        } else {
            reporter.setPromptText("Schriftführer");
        }
    }

    public void updateStyle(ActionEvent actionEvent) {
        File themeFile = new File("styles.css");
        String theme1Url = themeFile.toURI().toString();

        if (!themeFile.exists()) {
            System.out.println(theme1Url + " does not exist");
            return;
        }

        System.out.println("Style file " + themeFile.getAbsolutePath() + " found. Applying...");
        System.out.println("Old CSS registered in rootGridPane:");
        System.out.println(" " + String.join("\n ", rootGridPane.getStylesheets()));
        System.out.println("Update old styles.css with:");
        System.out.println(" " + theme1Url);

        rootGridPane.getStylesheets().removeIf(style -> style.endsWith("/styles.css"));
        rootGridPane.getStylesheets().add(theme1Url);
    }

    public void deleteGuest(KeyEvent event) {
        if (event.getCode().equals(KeyCode.DELETE)) {
            List<String> toBeDeleted = guestList.getSelectionModel().getSelectedItems();

            guestList.getItems().removeAll(toBeDeleted);
        }
    }
}
