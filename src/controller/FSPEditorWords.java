package controller;

import java.util.Map;

import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * This class contains the methods of the window which manages the <code>words</code>.
 * It is possible to add and delete difficult words.
 */
public class FSPEditorWords {

    public static final String FILENAME = "words.txt";
    public TextField searchTextField;
    public TextField addTextField;

    public ListView<String> wordList; // TODO list view makes no sense
    private Map<String, String> words;
    private FilteredList<String> filteredWords;

    @FXML
    public void initialize() {
        throw new NotImplementedException();
    }

    /**
     * Adds <code>newDifficultWord</code> to <code>observableWords</code> and words.xml.
     *
     * @param actionEvent
     */
    public void addButtonClicked(ActionEvent actionEvent) {
        throw new NotImplementedException();
    }

    /**
     * Deletes selected word from <code>observableWords</code> and words.xml.
     *
     * @param actionEvent
     */
    public void deleteButtonClicked(ActionEvent actionEvent) {
        throw new NotImplementedException();
    }

    /**
     * Imports list of words from <code>FSPEditor</code> class and loads words from file
     * to <code>words</code>.
     *
     * @param words
     */
    public void setWords(Map<String, String> words) {
        throw new NotImplementedException();
    }
}
