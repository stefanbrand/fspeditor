package model;

import controller.FSPEditorTreeChanger;
import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.VBox;

/**
 * Textfield that has references to the <code>PTreeNode</code> it is stored in and the <code>TreeItem</code> that is
 * to be selected when the textfield gains focus.
 * The <code>PTreeNode</code> stored in the textfield does not have to match the <code>PTreeNode</code> stored in the
 * <code>TreeItem</code>.
 */
public class PTextField extends TextField {

    /**
     * String of spaces to emulate the tab character.
     */
    public static final int MARGIN_PER_INDENT = 15;

    private TreeItem<PTreeNode> navigationEntry;
    private PTreeNode pTreeNode;
    private boolean isCensored = false;

    private int depth = 0;

    public PTextField(String text, PTreeNode pTreeNode, int depth) {
        this.pTreeNode = pTreeNode;
        setDepth(depth);
        setText(text);

        focusedProperty().addListener(FSPEditorTreeChanger.textFieldFocusListener);

        setOnKeyPressed(FSPEditorTreeChanger::handleTextFieldKeyPressedEvent);
        setOnKeyReleased(FSPEditorTreeChanger::handleTextFieldKeyTypedEvent);
    }

    public PTextField(PTreeNode pTreeNode, int depth) {
        this("", pTreeNode, depth);
    }

    public TreeItem<PTreeNode> getNavigationEntry() {
        return navigationEntry;
    }

    public void setNavigationEntry(TreeItem<PTreeNode> navigationEntry) {
        this.navigationEntry = navigationEntry;
    }

    public PTreeNode getpTreeNode() {
        return pTreeNode;
    }

    public void setpTreeNode(PTreeNode pTreeNode) {
        this.pTreeNode = pTreeNode;
    }

    public void increaseDepth() {
        setDepth(getDepth() + 1);
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;

        // censor if parent is censored
        if (pTreeNode.getParent() != null) {
            if (!isCensored && pTreeNode.getParent().isCensored()) {
                setCensored(true);
                pTreeNode.propagateCensored();
            }
        }

        updateStyle();
    }

    public void decreaseDepth() throws IllegalStateException {
        if (getDepth() == 0) {
            throw new IllegalStateException("Negative depths are not supported!");
        }

        setDepth(getDepth() - 1);
    }

    public void setCensored(boolean isCensored) {
        this.isCensored = isCensored;

        updateStyle();
    }

    public void updateStyle() {
        VBox.setMargin(this, new Insets(0, 0, 0, Math.max(0, MARGIN_PER_INDENT * (depth - 1))));

        getStyleClass().clear();

        getStyleClass().add("root");

        switch (depth) {
            case 0:
                setEditable(false);
                getStyleClass().add("section");
                break;
            case 1:
                setEditable(true);
                getStyleClass().add("headline");
                break;
            default:
                setEditable(true);
        }

        if (isCensored) {
            getStyleClass().add("censored");
        }
    }

    public boolean isCensored() {
        return isCensored;
    }
}
