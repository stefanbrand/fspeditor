package model;

import java.util.Objects;

/**
 * Stores the metadata that describes a redmine document.<br>
 * The values do not have a specific format.
 */
public class DocumentMetadata {

    private String title = "";
    private String from = "";
    private String to = "";
    private String place = "";
    private String reporter = "";
    private boolean femaleReporter = false;
    private String date = "";

    /**
     * Gets the title of the document.
     *
     * @return the title of the document
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the document.
     *
     * @param title
     *         the title of the document
     *
     * @throws java.lang.NullPointerException
     *         <code>title</code> was <code>null</code>
     */
    public void setTitle(String title) {
        Objects.requireNonNull(title, "title must not be null!");

        this.title = title;
    }

    /**
     * Gets the time when the meeting started.
     *
     * @return the time when the meeting started
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the time when the meeting started.
     *
     * @param from
     *         the time when the meeting started
     *
     * @throws java.lang.NullPointerException
     *         <code>from</code> was <code>null</code>
     */
    public void setFrom(String from) {
        Objects.requireNonNull(from, "from must not be null!");

        this.from = from;
    }

    /**
     * Gets the time when the meeting ended.
     *
     * @return the time when the meeting ended
     */
    public String getTo() {
        return to;
    }

    /**
     * Sets the time when the meeting ended
     *
     * @param to
     *         the time when the meeting ended
     *
     * @throws java.lang.NullPointerException
     *         <code>to</code> was <code>null</code>
     */
    public void setTo(String to) {
        Objects.requireNonNull(to, "to must not be null!");

        this.to = to;
    }

    /**
     * Gets the place where the meeting was held.
     *
     * @return the place
     */
    public String getPlace() {
        return place;
    }

    /**
     * Sets the place where the meeting was held.
     *
     * @param place
     *         the place
     *
     * @throws java.lang.NullPointerException
     *         <code>place</code> was <code>null</code>
     */
    public void setPlace(String place) {
        Objects.requireNonNull(place, "place must not be null!");

        this.place = place;
    }

    /**
     * Gets the reporter.
     *
     * @return the reporter
     */
    public String getReporter() {
        return reporter;
    }

    /**
     * Sets the reporter.
     *
     * @param reporter
     *         the reporter
     *
     * @throws java.lang.NullPointerException
     *         <code>reporter</code> was <code>null</code>
     */
    public void setReporter(String reporter) {
        Objects.requireNonNull(reporter, "reporter must not be null!");

        this.reporter = reporter;
    }

    /**
     * Gets whether the reporter was female
     *
     * @return whether the reporter was female
     */
    public boolean isFemaleReporter() {
        return femaleReporter;
    }

    /**
     * Sets whether the reporter was female
     *
     * @param femaleReporter
     *         whether the reporter was female
     */
    public void setFemaleReporter(boolean femaleReporter) {
        this.femaleReporter = femaleReporter;
    }

    /**
     * Gets the date when the meeting was held.
     *
     * @return the date when the meeting was held
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the date when the meeting was held
     *
     * @param date
     *         when the meeting was held
     *
     * @throws java.lang.NullPointerException
     *         <code>date</code> was <code>null</code>
     */
    public void setDate(String date) {
        Objects.requireNonNull(date, "date must not be null!");

        this.date = date;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Title: ").append(title).append("\n");
        sb.append("Place: ").append(place).append("\n");
        sb.append("From: ").append(from).append("\n");
        sb.append("To: ").append(to).append("\n");
        sb.append("Date: ").append(date).append("\n");
        sb.append("Reporter: ").append(reporter).append("\n");
        sb.append("Is Female Reporter: ").append(femaleReporter ? "true" : "false");

        return sb.toString();
    }
}
