package model;

import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import javafx.scene.control.TreeItem;
import javafx.util.Pair;

/**
 * Parses a given Redmine source string. Expects a valid Redmine document.<br>
 * Every space and newline shown below is needed!<br>
 * Text that is to be inserted is wrapped in [] brackets.<br>
 * A valid document has the following structure:<br>
 * <p>
 * <pre>
 * h1. [Title]
 *
 * |_Datum_|_Ort_|_Beginn_|_Ende_|_Schriftführer_| // optional line; use _Schriftführerin_ for a female reporter
 * |[Date]|[Place]|[Starttime]|[Endtime]|[Reporter]| // only add when above line is given; otherwise don't add
 *
 * h3. Anwesend
 *
 * * [Name]
 * * [Name2] // one name is mandatory; every other name is optional; there must not be a newline between names
 *
 * h3. Gäste // this whole section is optional; the "h3. Gäste" line without any guests following is valid
 *
 * * [Guest1]
 * * [Guest2]
 *
 * h2. [Agenda Item 1] // h2. must be followed by a newline and then a #-node! **-nodes are illegal.
 *
 * # [Agenda Item 1.a] // a #-node must be followed (without newline) by another #-node or a child **-node
 * ** [Text Item 1.a.1] // a **-node must be followed by another **-node or a ***-node. ****-node is illegal.
 * *** [Text Item 1.a.1.1] // there must not be newlines between text items
 * *** [Text Item 1.a.1.2]
 * # [Agenda Item 1.b]
 * # [Agenda Item 1.c]
 *
 * h2. [Agenda Item 2] // empty h2. agenda items are valid
 *
 * h2. [Agenda Item 3] // there must be a newline before any new h2. agenda item
 *
 * # [Agenda Item 3.a]
 * ** [Text Item 3.a.1]
 * </pre>
 * <p>
 * Important:
 * <ul>
 * <li>Only the title should be in a h1.-node. The title must be over the "Anwesend" and "Gäste" section. Additional
 * h1.-nodes will be ignored.</li>
 * <li>After the "Anwesend" (present members) and "Gäste" (guests) sections are over, additional *- or h3.-nodes must
 * not occur.</li>
 * <li>All newlines in the above example are necessary! Additional newlines after existing newlines are valid.
 * Additional newlines without existing newlines are invalid!</li>
 * <li>At the moment there is not limit to which depth **-nodes can be cascaded.</li>
 * </ul>
 */
public class RedmineParser {

    /**
     * The maximum number of characters of the line that is printed when that line is skipped.
     */
    public static final int LOGGER_PREVIEW_LENGTH = 50;
    private static int lineCount = 0;
    private static StringBuilder log;
    private static DocumentMetadata metadata;
    private static List<String> presentMembers;
    private static List<String> guests;
    /**
     * If true guest will be added instead of present members.
     */
    private static boolean inGuestMode;

    /**
     * *-nodes after this is true will be ignored.
     */
    private static boolean presentMembersAndGuestsDone;

    /**
     * Parses the given redmine source code and gives the navigation tree view nodes, the title and adds the present
     * members.
     *
     * @param input
     *         the redmine source code
     * @param presentMembers
     *         The list of members attending the meeting. Must be an empty list! No new list will be created meaning
     *         that the present members will be added to this very list. List will be sorted lexicographic.
     * @param guests
     *         The list of members attending the meeting. Must be an empty list! No new list will be created meaning
     *         that the present members will be added to this very list. List will be sorted lexicographic.
     *
     * @return a pair the root navigation tree view node and the document metadata
     *
     * @throws java.lang.NullPointerException
     *         if <code>input</code> or <code>presentMembers</code> are <code>null</code>
     * @throws java.lang.IllegalArgumentException
     *         if <code>presentMembers</code> or <code>guests</code> are not empty lists
     */
    public static Pair<TreeItem<PTreeNode>, DocumentMetadata> parse(String input, List<String> presentMembers,
            List<String> guests) {
        Objects.requireNonNull(input, "input must not be null!");
        Objects.requireNonNull(presentMembers, "presentMembers must not be null!");
        Objects.requireNonNull(guests, "guests must not be null!");
        if (!presentMembers.isEmpty()) {
            throw new IllegalArgumentException("presentMembers must be an empty list!");
        }
        if (!guests.isEmpty()) {
            throw new IllegalArgumentException("guests must be an empty list!");
        }

        System.out.println("Starting to parse given Redmine document...");

        PTreeNode rootPTree = new PTreeNode(null, "root");
        TreeItem<PTreeNode> rootTreeView = new TreeItem<>(rootPTree);

        Scanner scanner = new Scanner(input);

        log = new StringBuilder();

        RedmineParser.presentMembers = presentMembers;
        RedmineParser.guests = guests;
        RedmineParser.metadata = new DocumentMetadata();

        inGuestMode = false;
        presentMembersAndGuestsDone = false;

        // get children
        String lastLine = addChildren(rootPTree, "root", rootTreeView, scanner, false);

        log.append("LastLine: ").append(lastLine);

        System.out.println(log.toString()); // TODO remove debug

        presentMembers.sort(String::compareTo);
        guests.sort(String::compareTo);

        return new Pair<>(rootTreeView, metadata);
    }

    /**
     * Adds the title, the present members and the guests.
     *
     * @param line
     *         the line to process
     *
     * @return Whether the given <code>line</code> was skipped (<code>true</code>) or processed (<code>false</code>).
     * Empty lines will always be counted as processed.
     */
    private static boolean addPeopleAndMetadata(String line) {
        // handle empty lines (they have semantics!)
        if (line.isEmpty()) {
            if (presentMembersAndGuestsDone || presentMembers.isEmpty()) {
                return false;
            }

            if (guests.isEmpty() && !inGuestMode) {
                System.out.println("Present members complete. " + presentMembers.size() + " found.");
                System.out.println("Guests are being read...");
                inGuestMode = true;
            }

            return false;
        }

        // try to find title
        if (line.startsWith("h1. ")) {
            if (metadata.getTitle().isEmpty()) {
                metadata.setTitle(line.replaceFirst("h1\\.", ""));
                System.out.println("Title found: " + metadata.getTitle());
                return false;
            } else {
                System.out.println("A h1-element was found but there already is a title: " + line);
                return true;
            }
        }

        // try to find the table with date, place, reporter, ...
        if (line.startsWith("|") && !presentMembersAndGuestsDone) {
            String[] parts = line.split("\\|");
            final int expectedColumnCount = 6;

            if (parts.length != expectedColumnCount) {
                log.append("\nTable in metadata section did not have the right amount of columns! (%d instead of %d)",
                        parts.length, expectedColumnCount);
                return true;
            }

            if (line.startsWith("|_Datum_")) {
                metadata.setFemaleReporter("_Schriftführerin_".equals(parts[5]));
            } else {
                metadata.setDate(parts[1]);
                metadata.setPlace(parts[2]);
                metadata.setFrom(parts[3]);
                metadata.setTo(parts[4]);
                metadata.setReporter(parts[5]);
                System.out.println("Metadata complete:");
                System.out.println(metadata.toString());
            }

            return false;
        }

        // try to find present members and guests
        if (line.startsWith("*") && !presentMembersAndGuestsDone) {
            String name = line.replaceFirst("\\*", "").trim();

            if (inGuestMode) {
                guests.add(name);
            } else {
                presentMembers.add(name);
            }

            return false;
        } else {
            return true;
        }
    }

    /**
     * Adds the children to the given <code>parent</code>.
     *
     * @param parent
     *         the <code>PTreeNode</code> to add the children to
     * @param parentType
     *         the type of the parent (h2., #, **, ***, ****, ...)
     * @param navigationEntry
     *         the <code>TreeItem</code> that is to be linked to the children
     * @param scanner
     *         the <code>Scanner</code> containing the redmine source string
     * @param parentIsCensored
     *         Whether or not the parent is censored. If the parent is censored, all children have to censored as well.
     *
     * @return the last unprocessed line of the scanner (see comment in implementation)
     */
    private static String addChildren(PTreeNode parent, String parentType, TreeItem<PTreeNode> navigationEntry,
            Scanner scanner, boolean parentIsCensored) {
        /*
         * Basic idea of the recursion: Read line and process if possible. The next line could be a child of the current
         * node so go into recursion and call addChildren. If the next line was no child then the addChildren could
         * not add new nodes to the tree but it has read the next line (scanner was altered) without processing the
         * line.
         * But it gives back the unprocessed line via the returned String.
         */
        if (!scanner.hasNextLine()) {
            log.append("EOF after ").append(lineCount).append(" lines\n");
            return "";
        }

        String line = scanner.nextLine().trim();
        lineCount++;

        // skip empty lines and lines with unknown prefix; look for title; read present members
        while (line.isEmpty() || !(line.startsWith("h2.") || line.startsWith("#") || line.startsWith("**"))) {
            boolean skipped = addPeopleAndMetadata(line);

            if (skipped) {
                log.append("Skipped line ").append(lineCount).append(": ")
                        .append(line.substring(0, (Math.min(line.length(), LOGGER_PREVIEW_LENGTH)))).append("\n");
            }

            if (scanner.hasNextLine()) {
                line = scanner.nextLine().trim();
                lineCount++;
            } else {
                log.append("EOF after ").append(lineCount).append(" lines\n");
                return "";
            }
        }

        if (!presentMembersAndGuestsDone) {
            // first "real" line finished present member and guest mode
            System.out.println("Guests complete. " + guests.size() + " found");
            presentMembersAndGuestsDone = true;
        }


        TreeItem<PTreeNode> curNavigationNode;
        PTreeNode curPTreeNode;
        PTextField curTextField;
        boolean loopWasEntered = false; // only check one node-type (h2., #, or **)
        boolean isCensored = false;

        while (line.startsWith("h2.") && "root".equals(parentType)) {
            loopWasEntered = true;
            String section = line.replaceFirst("h2\\.", "").trim();

            // check if censored
            if (section.length() > 2 && section.charAt(0) == '-' && section.charAt(section.length() - 1) == '-') {
                isCensored = true;
                section = section.substring(1, section.length() - 1); // strip hyphens
            }

            // add to PTree
            curPTreeNode = new PTreeNode(parent, section);
            parent.getChildren().add(curPTreeNode);
            curTextField = curPTreeNode.getTextField();
            curTextField.setDepth(0);

            if (parentIsCensored || isCensored) {
                curPTreeNode.setCensored(true);
            }

            // add to tree view
            curNavigationNode = new TreeItem<>(curPTreeNode);
            navigationEntry.getChildren().add(curNavigationNode);

            // link text field to tree view
            curTextField.setNavigationEntry(curNavigationNode);

            // try to find children
            line = addChildren(curPTreeNode, "h2.", curNavigationNode, scanner, isCensored);
        }

        if (loopWasEntered) {
            return line;
        }

        while (line.startsWith("#") && "h2.".equals(parentType)) {
            loopWasEntered = true;
            String headline = line.replaceFirst("#", "").trim();

            // check if censored
            if (headline.length() > 2 && headline.charAt(0) == '-' && headline.charAt(headline.length() - 1) == '-') {
                isCensored = true;
                headline = headline.substring(1, headline.length() - 1); // strip hyphens
            }

            // add to PTree
            curPTreeNode = new PTreeNode(parent, headline);
            parent.getChildren().add(curPTreeNode);
            curTextField = curPTreeNode.getTextField();
            curTextField.setDepth(1);

            if (parentIsCensored || isCensored) {
                curPTreeNode.setCensored(true);
            }

            // add to tree view
            curNavigationNode = new TreeItem<>(curPTreeNode);
            navigationEntry.getChildren().add(curNavigationNode);

            // link text field to tree view
            curTextField.setNavigationEntry(curNavigationNode);

            // try to find children
            line = addChildren(curPTreeNode, "#", curNavigationNode, scanner, isCensored);
        }

        if (loopWasEntered) {
            return line;
        }

        while (line.startsWith("**")) {
            String[] parts = line.split(" ");
            String prefix = parts[0];

            // test if another sibling is found. exit when child is found.
            if (!(("#".equals(parentType) && "**".equals(prefix) || (prefix.length() - parentType.length() == 1)))) {
                return line;
            }

            loopWasEntered = true;
            String input = line.substring(prefix.length()).trim();

            // check if censored
            if (input.length() > 2 && input.charAt(0) == '-' && input.charAt(input.length() - 1) == '-') {
                isCensored = true;
                input = input.substring(1, input.length() - 1); // strip hyphens
            }

            // add to PTree
            curPTreeNode = new PTreeNode(parent, input);
            parent.getChildren().add(curPTreeNode);
            curTextField = curPTreeNode.getTextField();
            curTextField.setDepth(prefix.length());

            if (parentIsCensored || isCensored) {
                curPTreeNode.setCensored(true);
            }

            // link text field to tree view
            curTextField.setNavigationEntry(navigationEntry);

            // try to find children
            line = addChildren(curPTreeNode, prefix, navigationEntry, scanner, isCensored);
        }

        return line;
    }

    public static String getLogOfLastParsing() {
        return log.toString();
    }
}
