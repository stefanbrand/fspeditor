package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PTreeNode {

    private PTreeNode parent;
    private List<PTreeNode> children;
    private String name;
    private PTextField textField;

    /**
     * Creates a new <code>PTreeNode</code> and initialized the values using the given parameters. If
     * <code>hasText</code> is <code>true</code> then a <code>TextArea</code> and <code>TextField</code> will be
     * created and the text of the <code>TextField</code> will be set to <code>name</code>.
     *
     * @param parent
     *         the parent of the node or <code>null</code> if it is the root node
     * @param name
     *         the name of the node that is displayed in the navigation tree
     */
    public PTreeNode(PTreeNode parent, String name) {
        this.parent = parent;
        this.name = name;
        this.children = new ArrayList<>();
        this.textField = new PTextField(name, this, 0);
    }

    /**
     * Gets the name of the node. If the node has a text field then the name will be the text in the text field.
     *
     * @return the name of the node
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this node.
     *
     * @param value
     *         the new name of this node
     */
    public void setName(String value) {
        Objects.requireNonNull(value, "value must not be null!");
        name = value;
    }

    public PTextField getTextField() {
        return textField;
    }

    /**
     * Gets the parent of the node.
     *
     * @return the node's parent
     */
    public PTreeNode getParent() {
        return parent;
    }

    /**
     * Sets the parent of the node.
     *
     * @param parent
     *         the new parent of the node
     */
    public void setParent(PTreeNode parent) {
        this.parent = parent;
    }

    /**
     * Gets the list of all children of the node.
     *
     * @return the list of the node's children
     */
    public List<PTreeNode> getChildren() {
        return children;
    }

    public String toHTML() {
        StringBuilder builder = new StringBuilder();
        builder.append("<body>");

        builder.append("<\\body>");
        return builder.toString();
    }

    /**
     * Gets the HTML of this node and all its children.
     *
     * @param builder
     *         the <code>StringBuilder</code> to write the HTML into
     *
     * @return the HTML of this node and all its children
     */
    private String toHTML(StringBuilder builder) {
        return null; // TODO implement
    }

    /**
     * Gets the LaTeX code of this node and all its children.
     *
     * @return the LaTeX code of this node and all its children
     */
    public String toLATEX() {
        return null; // TODO implement
    }

    /**
     * Gets the LaTeX code of this node and all its children.
     *
     * @param builder
     *         the <code>StringBuilder</code> to write the LaTeX code into</code>
     *
     * @return the LaTeX code of this node and all its children
     */
    private String toLATEX(StringBuilder builder) {
        return null; // TODO implement
    }

    /**
     * Gets the source code for Redmine of this node and all its children.
     *
     * @return the Redmine source code of this node and all its children
     */
    public String toRedmine() {
        StringBuilder stringBuilder = new StringBuilder();

        toRedmine(stringBuilder); // print the tree recursively

        return stringBuilder.toString().trim();
    }

    /**
     * Helper method that recursively adds the redmine code of this node and all its children to the given
     * <code>stringBuilder</code>.
     *
     * @param stringBuilder
     *         the <code>StringBuilder</code> to add the redmine code to
     */
    private void toRedmine(StringBuilder stringBuilder) {
        // add string of this node
        switch (getTextField().getDepth()) {
            case 0:
                stringBuilder.append("h2. " + censor(name) + "\n\n");
                break;
            case 1:
                stringBuilder.append("# " + censor(getTextField().getText().trim()) + "\n");
                break;
            default:
                for (int i = 0; i < getTextField().getDepth(); i++) {
                    stringBuilder.append("*");
                }
                stringBuilder.append(" " + censor(getTextField().getText().trim()) + "\n");
        }

        // add string of children
        getChildren().forEach(node -> node.toRedmine(stringBuilder));
    }

    /**
     * Puts censor hyphens around the <code>text</code> if necessary.
     *
     * @param text
     *         the text to censor
     */
    private String censor(String text) {
        return isCensored() ? "-" + text + "-" : text;
    }

    public boolean isCensored() {
        return textField.isCensored();
    }

    public void setCensored(boolean isCensored) {
        // only allow children to be in the same un-censored if parent is un-censored
        if (isCensored || getParent().isCensored() == isCensored) {
            textField.setCensored(isCensored);
        }
    }

    public void toggleCensored() {
        setCensored(!isCensored());
    }

    /**
     * Gives each descendant (children and their children...) of this node the same censored status as this node.
     */
    public void propagateCensored() {
        getChildren().forEach(child -> {
            child.setCensored(isCensored());
            child.propagateCensored();
        });
    }

    /**
     * Gets the name of the node that has been set in the constructor of the node.
     *
     * @return the name of the node
     */
    public String toString() {
        return name;
    }
}
