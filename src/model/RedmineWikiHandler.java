package model;

import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;

import org.cdmckay.coffeedom.Document;
import org.cdmckay.coffeedom.Element;
import org.cdmckay.coffeedom.input.SAXBuilder;
import org.cdmckay.coffeedom.output.XMLOutputter;
import org.glassfish.jersey.client.ClientConfig;

/**
 * Handles communication with the Redmine REST API to query and update the wiki.
 */
public class RedmineWikiHandler {

    public static final String XML_TEXT = "text";
    public static final String XML_TITLE = "title";
    public static final String XML_WIKI_PAGE = "wiki_page";
    public static final String XML_PARENT = "parent";
    private static final String WIKI_PATH = "wiki";
    private static final String WIKI_INDEX = "index.xml";
    private static final String API_HEADER = "X-Redmine-API-Key";
    private static final String CONFERENCE = "sitzung";

    private final String wikiName;

    private Client client;
    private WebTarget wiki;
    private Function<WebTarget, WebTarget> webTargetConfig;
    private Function<WebTarget, Invocation.Builder> invocationBuilder;
    private Consumer<Invocation.Builder> invocationConfig;

    /**
     * Constructs a new <code>RedmineWikiHandler</code> using the given <code>Settings</code> object for establishing
     * its connection.
     *
     * @param settings
     *         the <code>Settings</code> to be used
     */
    public RedmineWikiHandler(Settings settings) {
        this(settings, null);
    }

    /**
     * Constructs a new <code>RedmineWikiHandler</code> using the given <code>Settings</code> object for establishing
     * its connection. The given <code>ClientConfig</code> will be used to set up the <code>Client</code>.
     *
     * @param settings
     *         the <code>Settings</code> to be used
     * @param clientConfig
     *         the <code>ClientConfig</code> to be used
     */
    public RedmineWikiHandler(Settings settings, ClientConfig clientConfig) {
        wikiName = settings.getWikiName();

        if (clientConfig == null) {
            client = ClientBuilder.newClient();
        } else {
            client = ClientBuilder.newClient(clientConfig);
        }

        wiki = client.target(settings.getRestURL()).path(settings.getProjectName()).path(WIKI_PATH);

        webTargetConfig = target -> target;
        invocationBuilder = WebTarget::request;
        invocationConfig = b -> b.header(API_HEADER, settings.getApiKey());
    }

    /**
     * Downloads a list of XML <code>Element</code>s in the Redmine API format each representing a conference wiki
     * page.
     *
     * @return the list of wiki pages as XML elements
     */
    public List<Element> loadConferenceWikiPages() {
        List<Element> index = loadWikiIndex();
        Predicate<String> pageNameFilter = name -> name.toLowerCase().contains(CONFERENCE);
        Predicate<String> parentNameFilter = name -> name.equals(wikiName);

        return loadWikiPages(index, pageNameFilter, parentNameFilter);
    }

    /**
     * Downloads a list of XML <code>Element</code>s in the Redmine API format each representing a conference wiki
     * page. The wiki pages will be filtered by their title and the name of their parent using the given
     * <code>Predicate</code>s.
     *
     * @param index
     *         the XML Elements representing the wiki page index
     * @param pageNameFilter
     *         the filter for the page title
     * @param parentNameFilter
     *         the filter for the title of the parent
     *
     * @return the list of wiki pages as XML elements
     */
    private List<Element> loadWikiPages(List<Element> index, Predicate<String> pageNameFilter,
            Predicate<String> parentNameFilter) {
        Stream<Element> s = index.stream().filter(element -> {
            Element parent = element.getChild(XML_PARENT);

            if (parent == null) {
                return parentNameFilter.test("");
            } else {
                return parentNameFilter.test(parent.getAttributeValue(XML_TITLE));
            }
        });
        s = s.filter(element -> pageNameFilter.test(element.getChildText(XML_TITLE)));
        List<Element> pagesToGet = s.collect(Collectors.toList());
        List<Element> pages = new LinkedList<>();

        SAXBuilder saxBuilder = new SAXBuilder();
        Document pageDocument;
        WebTarget page;
        Response response;

        for (Element pageToGet : pagesToGet) {
            page = wiki.path(pageToGet.getChildText(XML_TITLE) + ".xml");
            response = getBuilder(page).get();

            if (response.getStatus() != 200) {
                System.err.println("Could not load the wiki index. Status: " + response.getStatus());
                continue;
            }

            try {
                pageDocument = saxBuilder.build(new StringReader(response.readEntity(String.class)));
            } catch (Exception e) {
                System.err.println("Exception while parsing the wiki index. " + e);
                continue;
            }

            pages.add(pageDocument.getRootElement());
        }

        return pages;
    }

    /**
     * Downloads a list of XML <code>Element</code>s in the Redmine API format each representing an index entry
     * describing a wiki page.
     *
     * @return the list of XML <code>Elements</code> in the Redmine API format representing the wiki pages
     */
    public List<Element> loadWikiIndex() {
        WebTarget index = wiki.path(WIKI_INDEX);
        Response response = getBuilder(index).get();

        if (response.getStatus() != 200) {
            System.err.println("Could not load the wiki index. Status: " + response.getStatus());
            return new LinkedList<>();
        }

        SAXBuilder saxBuilder = new SAXBuilder();
        Document indexDoc;

        try {
            indexDoc = saxBuilder.build(new StringReader(response.readEntity(String.class)));
        } catch (Exception e) {
            System.err.println("Exception while parsing the wiki index. " + e);
            return new LinkedList<>();
        }

        Element root = indexDoc.getRootElement();

        return root.getChildren(XML_WIKI_PAGE);
    }

    /**
     * Uses <code>webTargetConfig</code>, <code>invocationBuilder</code>, and <code>invocationConfig</code> to
     * derive an <code>Invocation.Builder</code> from the given <code>WebTarget</code>.
     *
     * @param target
     *         the <code>WebTarget</code> to be used
     *
     * @return the resulting <code>Invocation.Builder</code>
     */
    private Invocation.Builder getBuilder(WebTarget target) {
        WebTarget configured = webTargetConfig.apply(target);
        Invocation.Builder builder = invocationBuilder.apply(configured);
        invocationConfig.accept(builder);

        return builder;
    }

    /**
     * Downloads a list of XML <code>Element</code>s in the Redmine API format each representing a conference wiki
     * page. The wiki pages will be filtered by their title and the name of their parent using the given
     * <code>Predicate</code>s.
     *
     * @param pageNameFilter
     *         the filter for the page title
     * @param parentNameFilter
     *         the filter for the title of the parent
     *
     * @return the list of wiki pages as XML elements
     */
    public List<Element> loadWikiPages(Predicate<String> pageNameFilter, Predicate<String> parentNameFilter) {
        return loadWikiPages(loadWikiIndex(), pageNameFilter, parentNameFilter);
    }

    /**
     * Uploads a new page with <code>title</code> and the given content.
     * The new page will be a under the root page of the wiki.
     *
     * @param title
     *         the title of the new wiki page
     * @param content
     *         the content of the new wiki page
     */
    public void pushPage(String title, String content) {
        Element rootElement = new Element(XML_WIKI_PAGE);
        rootElement.addContent(new Element(XML_TITLE).setText(title));
        rootElement.addContent(new Element(XML_PARENT).setAttribute(XML_TITLE, wikiName));
        rootElement.addContent(new Element(XML_TEXT).setText(content));

        String xml = new XMLOutputter().outputString(rootElement);
        WebTarget newPage = wiki.path(title + ".xml");
        Invocation.Builder builder = getBuilder(newPage);

        Response response = builder.put(Entity.xml(xml));

        int status = response.getStatus();
        if (!(status == 200 || status == 201)) {
            System.err.println("Could not PUT the new wiki page. Status: " + response.getStatus());
            System.err.println(response.readEntity(String.class));
        }
    }

    /**
     * Sets the <code>webTargetConfig</code> that is applied when establishing a connection to the wiki.
     *
     * @param webTargetConfig
     *         the new <code>webTargetConfig</code>
     *
     * @return <code>this</code>
     */
    public RedmineWikiHandler setWebTargetConfig(Function<WebTarget, WebTarget> webTargetConfig) {
        this.webTargetConfig = webTargetConfig;
        return this;
    }

    /**
     * Sets the <code>invocationBuilder</code> that is used the derive a <code>Invocation.Builder</code> from the
     * <code>WebTarget</code> when establishing a connection.
     *
     * @param invocationBuilder
     *         the new <code>invocationBuilder</code>
     *
     * @return <code>this</code>
     */
    public RedmineWikiHandler setInvocationBuilder(Function<WebTarget, Invocation.Builder> invocationBuilder) {
        this.invocationBuilder = invocationBuilder;
        return this;
    }

    /**
     * Adds a configuration step to that is applied to the <code>Invocation.Builder</code> when establishing a
     * connection.
     *
     * @param invocationConfig
     *         the added <code>invocationConfig</code>
     *
     * @return <code>this</code>
     */
    public RedmineWikiHandler addInvocationConfig(Consumer<Invocation.Builder> invocationConfig) {
        this.invocationConfig = builder -> {
            this.invocationConfig.accept(builder);
            invocationConfig.accept(builder);
        };
        return this;
    }
}
