package model;

/**
 * Exception that shows that the document was not created at all or lacks important values.
 */
public class EmptyDocumentException extends Exception {

    public EmptyDocumentException() {
    }

    public EmptyDocumentException(String message) {
        super(message);
    }

    public EmptyDocumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyDocumentException(Throwable cause) {
        super(cause);
    }

    public EmptyDocumentException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
