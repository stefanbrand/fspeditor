package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Settings")
public class Settings {

    public static String FILENAME = "settings.xml";
    private static XStream saveLoadStream;

    static {
        saveLoadStream = new XStream();
        saveLoadStream.processAnnotations(Settings.class);
    }

    private String restURL = "";
    private String projectName = "";
    private String apiKey = "";
    private String wikiName = "";

    public static Settings load() {
        return load(new File(FILENAME));
    }

    public static Settings load(File saveFile) {

        try {
            return (Settings) saveLoadStream.fromXML(saveFile);
        } catch (Exception e) {
            System.err.println("Error de-serialising: " + e);
            return null;
        }
    }

    public String getRestURL() {
        return restURL;
    }

    public void setRestURL(String restURL) {
        this.restURL = restURL;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getWikiName() {
        return wikiName;
    }

    public void setWikiName(String wikiName) {
        this.wikiName = wikiName;
    }

    public void save() throws IOException {
        save(new File(FILENAME));
    }

    public void save(File saveFile) throws IOException {
        saveLoadStream.toXML(this, new FileWriter(saveFile));
    }

    public boolean allSettingsGiven() {
        return !(restURL.isEmpty() || projectName.isEmpty() || apiKey.isEmpty() || wikiName.isEmpty());
    }
}
